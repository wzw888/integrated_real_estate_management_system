//对话框
function showDialog(div) {
    var maskHight = $(document).height();
    var maskWidth = $(document).width();
    var dialogTop = (maskHight / 2) - ($("#" + div).height() / 2);
    var dialogLeft = (maskWidth / 2) - ($("#" + div).width() / 2);
    $("#dialog-overlay").css({height: maskHight, width: maskWidth}).show();
    $("#" + div).css({top: dialogTop, left: dialogLeft}).show();
}

//提示框
function showTip(msg, dey) {
    var maskHight = $(document).height();
    var maskWidth = $(document).width();
    var dialogTop = (maskHight / 2) - ($("#tip").height() / 2);
    var dialogLeft = (maskWidth / 2) - ($("#tip").width() / 2);
    $("#dialog-overlay").css({height: maskHight, width: maskWidth}).show();
    $("#tip").css({top: dialogTop, left: dialogLeft}).show();
    $("#tip").html("<p>" + msg + "</p>");
    setTimeout(function () {
        $("#dialog-overlay").hide();
        $("#tip").hide();
    }, dey);
}

//取消弹出层
function Cancel() {
    $("#dialog-overlay").hide();
    $("#layer").hide();
}

function selectAll(cbx) {
    cbx = $(cbx);
    if (cbx.attr("isall")) {
        //none
        $("table.table :checkbox").prop("checked", false);
        cbx.removeAttr("isall");

    } else {
        //all
        $("table.table :checkbox").prop("checked", true);
        cbx.attr("isall", "isall");
    }
}

function deleteAll(action) {
    $("#tipName").text("确定删除吗？");
    $("#tipButton").click(function () {
        window.form.action = action;
        window.form.submit();
    });
    showDialog('layer');
}

function pagechange() {
    if (arguments.length = 1) {
        $("#pageIndex").val(arguments[0]);
    }
    document.form.submit();
}

function showUserItem() {
    var text = "使用协议<br/>" +
        "欢迎来到房屋管理系统 ！房屋管理系统是一个迷你博客社区，允许用户创建个人空间，用网页、MSN/Gtalk/QQ、手机短信和手机WAP等多种方式发布个人消息并接收朋友消息。<br/>" +
        "我们的口号是“房屋管理系统，随时随地记录与分享！”。<br/>" +
        "用户在接受房屋管理系统服务之前，请务必仔细阅读本使用协议（以下简称“本协议”）并同意本协议。<br/>" +
        "用户直接或间接通过各类方式（例如房屋管理系统API等）使用房屋管理系统的服务和数据的行为，都将被视作已无条件接受本协议所涉全部内容；若用户对本协议的任何条款有异议，请停止使用房屋管理系统所提供的全部服务。<br/>" +
        "房屋管理系统 用户指在本站注册的用户。用户将拥有设置个人页面、发布消息、收发站内私信等基本权利。<br/>" +
        "一旦注册成为房屋管理系统用户，即代表你保证：<br/>" +
        "（a）你发布或转载的内容符合中国法律或社会公德；<br/>" +
        "（b）不得干扰、损害和侵犯房屋管理系统网的各种合法权利与利益；<br/>" +
        "（c）你在使用房屋管理系统服务时不会违反房屋管理系统一切有效的管理办法和规定。<br/>" +
        "房屋管理系统用户的个人资料受到房屋管理系统的保护。房屋管理系统承诺不会在未获得用户许可的情况下擅自将用户的个人资料信息出租或出售给任何第三方，但以下情况除外：<br/>" +
        "（a）用户同意让第三方共享资料；<br/>" +
        "（b）用户同意公开其个人资料，享受相应的产品和服务；<br/>" +
        "（c）房屋管理系统需要听从法庭传票、法律命令或遵循法律程序；<br/>" +
        "（d）房屋管理系统发现用户违反了本站服务条款或本站其它使用规定。更多关于隐私保护的内容请查看房屋管理系统的隐私声明。<br/>" +
        "房屋管理系统仅为用户发布的内容提供存储空间。因网络状况、通讯线路、第三方网站或管理部门的要求等任何原因导致不能正常使用房屋管理系统服务，房屋管理系统不承担任何法律责任。<br/>" +
        "房屋管理系统用户帐号仅代表用户个人，由于用户发布消息或者其他站上交流引起的法律上的或者经济上的责任，房屋管理系统不予承担其中任何形式的责任，用户的言论文责自负。<br/> " +
        "房屋管理系统用户在设置帐号资料、发表消息内容或使用其他任何房屋管理系统服务，均不能对他人进行骚扰或人身攻击、使用侮辱性言辞、引起不必要的纠纷、影射党和国家领导人、违反国家法律或含有其他不恰当的内容。如果违犯上述情形，将视情况予以警告、封禁部分权限直至删除帐号。<br/>" +
        "房屋管理系统用户不得利用本站帐号、信件、个人资料、插件等服务进行直接或者间接商业宣传，否则将视情节轻重给予警告、封禁部分权限直至删除帐号的处罚；<br/>" +
        "对于蓄意为某一商业机构进行多个商业宣传的，将视情节轻重予以警告、封禁部分权限直至删除帐号的处罚。<br/>" +
        "房屋管理系统用户不得手动或者使用特殊程序对本站系统进行恶意攻击。<br/>" +
        "对于恶意攻击系统并可能危及系统稳定运行的用户，直接删除帐号。<br/>" +
        "房屋管理系统用户有权力和责任对站内出现的违反国家法律法规以及有关站规的事件进行举报。<br/>" +
        "房屋管理系统用户帐号仅代表使用者个人，不得转借或共用帐号。违者将视情节受到警告、封禁部分权限直至删除帐号的处罚。<br/>" +
        "对于有意泄露帐号密码者，视同共用帐号处理。<br/>" +
        "未经具有相关所有权所有者之事先书面同意，房屋管理系统用户不得以任何方式上传、发布、修改、传播或复制任何受著作权保护的材料、商标或属于其他人的专有信息。<br/>" +
        "如果收到任何著作权人或其合法代表发给房屋管理系统的适当通知后，房屋管理系统将移除该等侵犯他人著作权的内容，并保留移交司法机关处理的权利。<br/>" +
        "房屋管理系统用户不得通过头像、用户名、昵称、网址、自述等个人资料冒充他人身份发布信息。如果遇到上述情形，房屋管理系统保留收回帐号的权利。<br/>" +
        "个人或单位如认为房屋管理系统上存在侵犯自身合法权益的内容，可准备好具有法律效应的证明材料，及时联系房屋管理系统网。 <br/>" +
        "房屋管理系统有权随时修改本协议中的任何条款。任何修改自发布之时起生效。<br/>" +
        "如果你在修改发布后继续使用房屋管理系统的服务，即表示你同意遵守对本协议所作出的修改。<br/>" +
        "欢迎随时查看本协议，以确保了解所有相关的最新修改。如果你不同意相关修改，请你离开本站并立即停止使用本站服务。<br/>" +
        "本协议解释权归房屋管理系统，有关事项与其他声明或协议互为补充。<br/>" +
        "若有冲突，以房屋管理系统公布的最新协议为准。本协议自公布之日起施行。"
    document.getElementById('tipName').innerHTML = text;
    $("#tipName").css({"line-height": "20px", "text-align": "left", "margin-left": "20px"});
    $("#layer").css({"width": "90%"});
    $("#tipButton").hide();
    $("#cancel").text("确定");
    showDialog('layer');
}