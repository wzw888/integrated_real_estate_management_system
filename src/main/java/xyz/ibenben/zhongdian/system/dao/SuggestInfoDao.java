package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.SuggestInfo;

/**
 * 意见及建议记录Dao类
 * 提供了一些基本的服务
 * 是用户在提交意见或建议时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SuggestInfoDao extends BaseDao<SuggestInfo> {
    //意见及建议
}
