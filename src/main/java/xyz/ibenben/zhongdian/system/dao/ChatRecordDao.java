package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.ChatRecord;

/**
 * 聊天记录Dao类
 * 提供了一些基本的服务
 * 是用户在互相聊天时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ChatRecordDao extends BaseDao<ChatRecord> {
    //聊天记录
}
