package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.Log;

/**
 * 日志Dao类
 * 提供了一些基本的服务
 * 记录日志到数据库
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface LogDao extends BaseDao<Log> {
    //日志
}
