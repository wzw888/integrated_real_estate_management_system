package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.CompanyInfo;

/**
 * 公司信息Dao类
 * 提供了一些基本的服务
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface CompanyInfoDao extends BaseDao<CompanyInfo> {
    //公司信息
}
