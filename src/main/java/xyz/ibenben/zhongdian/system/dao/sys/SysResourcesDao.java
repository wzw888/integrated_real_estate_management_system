package xyz.ibenben.zhongdian.system.dao.sys;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.sys.SysResources;

/**
 * 系统资源Dao类
 * 系统级资源管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SysResourcesDao extends BaseDao<SysResources> {
    //系统资源
}
