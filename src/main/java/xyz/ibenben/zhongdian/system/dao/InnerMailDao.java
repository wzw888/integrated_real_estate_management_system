package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.InnerMail;

/**
 * 站内信Dao类
 * 提供了一些基本的服务
 * 主要用于用户之间通信使用此类来维护
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface InnerMailDao extends BaseDao<InnerMail> {
    //站内信
}
