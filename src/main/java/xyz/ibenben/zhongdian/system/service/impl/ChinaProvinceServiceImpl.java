package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.dao.ChinaProvinceDao;
import xyz.ibenben.zhongdian.system.entity.ChinaProvince;
import xyz.ibenben.zhongdian.system.service.ChinaProvinceService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 省市服务实现类
 * 此类提供对省市查询的服务，是三级联动里的第一级
 * 提供了一些基本的服务，如通过代码查列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ChinaProvinceServiceImpl extends AbstractServiceImpl<ChinaProvince> implements ChinaProvinceService {
    @Resource
    private ChinaProvinceDao chinaProvinceDao;

    /**
     * findNameByCode省份记录
     *
     * @param provinceCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode省份记录")
    public String findNameByCode(Long provinceCode) {
        //返回结果
        if (null != provinceCode) {
            //组装实体
            ChinaProvince province = new ChinaProvince();
            province.setProvinceCode(provinceCode);
            ChinaProvince result = chinaProvinceDao.selectOne(province);
            return result.getProvinceName();
        }
        return "";
    }

    /**
     * 通过名称找代码
     *
     * @param name 名称
     * @return 返回值
     */
    @Override
    public List<Long> findCodeByName(String name) {
        List<Long> list = new ArrayList<>();
        //返回结果
        if (null != name && !"".equals(name)) {
            //组装实体
            ChinaProvince province = new ChinaProvince();
            province.setProvinceName(name);
            List<ChinaProvince> result = chinaProvinceDao.select(province);
            if (result != null) {
                for (ChinaProvince chinaProvince : result) {
                    list.add(chinaProvince.getProvinceCode());
                }
            }
        }
        return list;
    }
}
