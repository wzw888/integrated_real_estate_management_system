package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.dao.DealRecordInfoDao;
import xyz.ibenben.zhongdian.system.entity.DealRecordInfo;
import xyz.ibenben.zhongdian.system.form.DealRecordInfoForm;
import xyz.ibenben.zhongdian.system.mapper.DealRecordInfoMapper;
import xyz.ibenben.zhongdian.system.service.DealRecordInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 成交记录信息服务实现类
 * 提供了一些基本的服务，如根据主键查找成交记录信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class DealRecordInfoServiceImpl extends AbstractServiceImpl<DealRecordInfo> implements DealRecordInfoService {

    @Resource
    private DealRecordInfoDao dealRecordInfoDao;
    @Resource
    private DealRecordInfoMapper dealRecordInfoMapper;

    @Override
    public List<DealRecordInfo> findAll(DealRecordInfoForm dealRecordInfoForm) {
        return dealRecordInfoMapper.findAll(dealRecordInfoForm);
    }

    @Override
    public DealRecordInfo findByKey(Long id) {
        return dealRecordInfoMapper.findByKey(id);
    }
}
