package xyz.ibenben.zhongdian.system.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.dao.HouseBookingDao;
import xyz.ibenben.zhongdian.system.entity.HouseBooking;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.entity.HouseInfo;
import xyz.ibenben.zhongdian.system.entity.enums.HouseTypeEnum;
import xyz.ibenben.zhongdian.system.form.HouseBookingForm;
import xyz.ibenben.zhongdian.system.mapper.HouseBookingMapper;
import xyz.ibenben.zhongdian.system.service.HouseBookingService;
import xyz.ibenben.zhongdian.system.service.HouseImageService;
import xyz.ibenben.zhongdian.system.service.HouseInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 房屋预售服务实现类
 * 提供了一些基本的服务，如根据名称查记录、获取记录带图像等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class HouseBookingServiceImpl extends AbstractServiceImpl<HouseBooking> implements HouseBookingService {
    @Resource
    private HouseBookingDao houseBookingDao;
    @Resource
    private HouseBookingMapper houseBookingMapper;
    @Resource
    private HouseInfoService houseInfoService;
    @Resource
    private HouseImageService houseImageService;

    /**
     * save预售信息
     *
     * @param entity  实体
     * @param request 请求
     * @return 是否保存成功
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "save预售信息")
    public int save(HouseBooking entity, HttpServletRequest request) {
        //组装房屋实体
        HouseInfo info = entity.getInfo();
        info.setHouseName(entity.getName());
        info.setAddress(entity.getAddress());
        info.setProvince(entity.getProvince().toString());
        info.setCity(entity.getCity().toString());
        info.setRegion(entity.getRegion().toString());
        info.setType(HouseTypeEnum.BOOKING);
        //保存房屋信息
        houseInfoService.save(info, request);
        //保存预售信息
        entity.setHouseId(info.getId());
        entity.setHouseArea(info.getHouseArea());
        return this.save(entity, request);
    }

    /**
     * updateAll预售信息
     *
     * @param entity  实体
     * @param request 请求
     * @return 是否更新成功
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "updateAll预售信息")
    public int updateAll(HouseBooking entity, HttpServletRequest request) {
        //组装房屋信息
        HouseInfo info = entity.getInfo();
        info.setHouseName(entity.getName());
        info.setAddress(entity.getAddress());
        info.setProvince(entity.getProvince().toString());
        info.setCity(entity.getCity().toString());
        info.setRegion(entity.getRegion().toString());
        //更新房屋信息
        houseInfoService.updateAll(info, request);
        //更新预售信息
        entity.setHouseArea(info.getHouseArea());
        return this.updateAll(entity, request);
    }

    /**
     * findByName预售信息
     *
     * @param name 预售房名称
     * @return 预售房列表
     */
    @Override
    @SystemServiceLog(description = "findByName预售信息")
    public List<HouseBooking> findByName(String name) {
        //根据预售名称查询预售列表
        Example example = new Example(HouseBooking.class);
        Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(name)) {
            criteria.andEqualTo("name", name);
        }
        return houseBookingDao.selectByExample(example);
    }

    /**
     * findOneWithImage预售信息
     *
     * @param id 房屋主键
     * @return 预售信息
     */
    @Override
    @SystemServiceLog(description = "findOneWithImage预售信息")
    public HouseBooking findOneWithImage(Long id) {
        HouseBooking book = houseBookingDao.selectByPrimaryKey(id);
        if (book.getHouseId() != null) {
            //为预售增加房屋信息
            HouseInfo info = houseInfoService.findOneWithImage(book.getHouseId());
            book.setInfo(info);
        }

        List<HouseImage> list = houseImageService.findByHouseId(id);
        if (list != null && !list.isEmpty()) {
            //为预售增加图像信息列表
            book.setList(list);
        }
        return book;
    }

    @Override
    public List<HouseBooking> findAll(HouseBookingForm houseBookingForm) {
        return houseBookingMapper.findAll(houseBookingForm);
    }
}
