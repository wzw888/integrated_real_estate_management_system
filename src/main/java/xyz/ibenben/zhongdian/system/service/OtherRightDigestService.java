package xyz.ibenben.zhongdian.system.service;

import xyz.ibenben.zhongdian.system.entity.OtherRightDigest;
import xyz.ibenben.zhongdian.system.form.OtherRightDigestForm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 他项权利摘要服务类
 * 提供了一些基本的服务，如根据房产证查找他项权利摘要记录列表等方法。
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface OtherRightDigestService extends IService<OtherRightDigest> {
    /**
     * 根据房产证查找他项权利摘要记录列表
     *
     * @param id 参数
     * @return 返回值
     */
    List<OtherRightDigest> findByOwnerId(Long id);

    /**
     * 根据拥有者主键删除记录
     *
     * @param id      拥有者主键
     * @param request 请求
     */
    void deleteByOwnerId(Long id, HttpServletRequest request);

    /**
     * 根据条件查询列表
     *
     * @param otherRightDigestForm 条件
     * @return 列表
     */
    List<OtherRightDigest> findAll(OtherRightDigestForm otherRightDigestForm);
}
