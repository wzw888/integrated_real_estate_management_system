package xyz.ibenben.zhongdian.system.service;

import xyz.ibenben.zhongdian.system.entity.PropertyOwnershipCertificate;
import xyz.ibenben.zhongdian.system.form.PropertyOwnershipCertificateForm;

import java.util.List;

/**
 * 产权服务类
 * 提供了一些基本的服务
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface PropertyOwnershipCertificateService extends IService<PropertyOwnershipCertificate> {
    /**
     * 根据条件查询列表
     *
     * @param propertyOwnershipCertificateForm 条件
     * @return 里恩
     */
    List<PropertyOwnershipCertificate> findAll(PropertyOwnershipCertificateForm propertyOwnershipCertificateForm);
}
