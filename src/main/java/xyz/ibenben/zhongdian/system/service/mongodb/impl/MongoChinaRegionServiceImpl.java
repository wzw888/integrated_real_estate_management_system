package xyz.ibenben.zhongdian.system.service.mongodb.impl;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.mongodb.MongoChinaRegion;
import xyz.ibenben.zhongdian.system.service.mongodb.MongoChinaRegionService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 区县服务实现类
 * 此类提供对区县查询的服务，是三级联动里的最后一级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class MongoChinaRegionServiceImpl extends AbstractMongoServiceImpl<MongoChinaRegion> implements MongoChinaRegionService {
    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * findListByCityCode区域记录
     *
     * @param cityCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findListByCityCode区域记录")
    public List<MongoChinaRegion> findListByCityCode(Long cityCode) {
        Query query = new Query(Criteria.where("cityCode").is(cityCode));
        return mongoTemplate.find(query, MongoChinaRegion.class);
    }

    /**
     * findNameByCode区域记录
     *
     * @param regionCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode区域记录")
    public String findNameByCode(Long regionCode) {
        String resultStr = "";
        if (null != regionCode) {
            Query query = new Query(Criteria.where("regionCode").is(regionCode));
            MongoChinaRegion result = mongoTemplate.findOne(query, MongoChinaRegion.class);
            if (result != null) {
                resultStr = result.getRegionName();
            }
        }
        return resultStr;
    }

}
