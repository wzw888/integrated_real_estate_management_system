package xyz.ibenben.zhongdian.system.service.mongodb.impl;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.mongodb.MongoChinaCity;
import xyz.ibenben.zhongdian.system.service.mongodb.MongoChinaCityService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 城市服务实现类
 * 此类提供对城市查询的服务，是三级联动里的第二级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class MongoChinaCityServiceImpl extends AbstractMongoServiceImpl<MongoChinaCity> implements MongoChinaCityService {
    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * findListByProvinceCode城市记录
     *
     * @param provinceCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findListByProvinceCode城市记录")
    public List<MongoChinaCity> findListByProvinceCode(Long provinceCode) {
        Query query = new Query(Criteria.where("provinceCode").is(provinceCode));
        return mongoTemplate.find(query, MongoChinaCity.class);
    }

    /**
     * findNameByCode城市记录
     *
     * @param cityCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode城市记录")
    public String findNameByCode(Long cityCode) {
        String resultStr = "";
        if (null != cityCode) {
            Query query = new Query(Criteria.where("cityCode").is(cityCode));
            MongoChinaCity result = mongoTemplate.findOne(query, MongoChinaCity.class);
            if (result != null) {
                resultStr = result.getCityName();
            }
        }
        return resultStr;
    }

}
