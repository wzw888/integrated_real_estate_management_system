package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import xyz.ibenben.zhongdian.system.dao.CompanyInfoDao;
import xyz.ibenben.zhongdian.system.entity.CompanyInfo;
import xyz.ibenben.zhongdian.system.form.CompanyInfoForm;
import xyz.ibenben.zhongdian.system.mapper.CompanyInfoMapper;
import xyz.ibenben.zhongdian.system.service.CompanyInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 公司信息服务实现类
 * 提供了一些基本的服务，如根据主键查找公司信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class CompanyInfoServiceImpl extends AbstractServiceImpl<CompanyInfo> implements CompanyInfoService {

    @Resource
    private CompanyInfoDao companyInfoDao;
    @Resource
    private CompanyInfoMapper companyInfoMapper;

    @Override
    public List<CompanyInfo> findByCompanyName(String companyName) {
        //通过房屋名称获取房屋信息列表
        Example example = new Example(CompanyInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("companyName", companyName);
        return companyInfoDao.selectByExample(example);
    }

    @Override
    public CompanyInfo findByKey(Long id) {
        return companyInfoMapper.findByKey(id);
    }

    @Override
    public List<CompanyInfo> findAll(CompanyInfoForm companyInfoForm) {
        return companyInfoMapper.findAll(companyInfoForm);
    }
}
