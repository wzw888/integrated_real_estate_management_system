package xyz.ibenben.zhongdian.system.entity.sys;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 系统用户实体类
 * 记录该表记录了用户名称，密码等字段
 * 表名是sys_user
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Setter
@Getter
@ToString
public class SysUser extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -8736616045315083846L;

    /**
     * 用户名称
     */
    @Column
    private String username;

    /**
     * 密码
     */
    @Column
    private String password;

    /**
     * 确认密码
     */
    @Transient
    private String cpassword;

    /**
     * 新密码
     */
    @Transient
    private String npassword;

    /**
     * 邮箱
     */
    @Column
    private String email;

    /**
     * 联系电话
     */
    @Column
    private String phone;

    /**
     * 地址
     */
    @Column
    private String address;

    /**
     * 所在省代码
     */
    @Column
    private Long province;

    /**
     * 所在市代码
     */
    @Column
    private Long city;

    /**
     * 所在区代码
     */
    @Column
    private Long region;

    /**
     * 前景色
     */
    @Column(name = "font_color")
    private String fontColor;

    /**
     * 背景色
     */
    @Column(name = "back_color")
    private String backColor;

    /**
     * 记住我
     */
    @Transient
    private String rememberMe;

    /**
     * 是否启用
     */
    @Column
    private Integer enable;

    /**
     * 类型
     */
    @Column
    private Boolean type;

    /**
     * 头像
     */
    @Column
    private String head;

    /**
     * 图像
     */
    @Transient
    private byte[] image;

    /**
     * 用户角色信息
     */
    @Transient
    private SysRole sysRole;

}