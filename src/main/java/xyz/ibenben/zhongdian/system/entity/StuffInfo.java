package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.StuffStatusEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 租赁物品信息实体类
 * 记录该表记录了租赁物品名称，物品数量等字段
 * 表名是stuff_info
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class StuffInfo extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5441997212327447070L;

    /**
     * 租赁主键
     */
    @Column
    private Long rentingId;

    /**
     * 租赁信息
     */
    @Transient
    private RentingHouse rentingHouse;

    /**
     * 物品名称
     */
    @NotNull(message = "物品名称不能为空")
    @Column
    private String name;

    /**
     * 物品数量
     */
    @NotNull(message = "物品数量不能为空")
    @Column
    private Integer number;

    /**
     * 物品状态
     */
    @NotNull(message = "物品状态不能为空")
    @Column
    private StuffStatusEnum status;

    /**
     * 备注
     */
    @Column
    private String memo;

}
