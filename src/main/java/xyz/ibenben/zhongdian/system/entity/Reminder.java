package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统提醒实体类
 * 记录该表记录了需要提醒的时间，需要提醒的内容等字段
 * 表名是reminder
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@Entity
@ToString
public class Reminder extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 4254130484943935732L;

    /**
     * 用户主键
     */
    @Column
    private Long userId;

    /**
     * 用户信息
     */
    @Transient
    private SysUser sysUser;

    /**
     * 需要提醒的内容
     */
    @Column
    private String title;

    /**
     * 开始时间
     */
    @Column
    private Date start;

    /**
     * 结束时间
     */
    @Column
    private Date end;

    /**
     * 是否为全日事件
     */
    @Column
    private Boolean allDay;

    /**
     * 是否需要提醒
     */
    @Column
    private Boolean remind;

    /**
     * 需要提醒的时间
     */
    @Column
    private Date remindTime;

    /**
     * 字体颜色
     */
    @Column
    private String textColor;

    /**
     * 背景颜色
     */
    @Column
    private String color;

}
