package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * 区县实体类
 * 记录该表记录了省份代码，城市代码等字段
 * 表名是china_region
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class ChinaRegion extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1080389537340314318L;

    /**
     * 省份代码
     */
    @Column
    private Long provinceCode;

    /**
     * 城市代码
     */
    @Column
    private Long cityCode;

    /**
     * 区县代码
     */
    @Column
    private Long regionCode;

    /**
     * 区县名称
     */
    @Column
    private String regionName;

}
