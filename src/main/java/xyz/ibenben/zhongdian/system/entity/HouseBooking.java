package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 房屋预售实体类
 * 记录该表记录了房屋主键，房屋名称等字段
 * 表名是house_booking
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class HouseBooking extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 8875856021674957449L;

    /**
     * 房屋主键
     */
    @Column
    private Long houseId;

    /**
     * 房屋名称
     */
    @Column
    private String name;

    /**
     * 房屋地址
     */
    @Column
    private String address;

    /**
     * 所在省代码
     */
    @Column
    private Long province;

    /**
     * 所在市代码
     */
    @Column
    private Long city;

    /**
     * 所在区代码
     */
    @Column
    private Long region;

    /**
     * 开盘价格
     */
    @Column
    private BigDecimal openPrice;

    /**
     * 建筑面积
     */
    @Column
    private BigDecimal houseArea;

    /**
     * 售楼电话
     */
    @Column
    private String phone;

    /**
     * 开发商
     */
    @Column
    private String developer;

    /**
     * 登记号码
     */
    @Column
    private String loginNumber;

    /**
     * 备注
     */
    @Column
    private String memo;

    /**
     * 房屋信息
     */
    @Transient
    private HouseInfo info = new HouseInfo();

    /**
     * 房屋图像
     */
    @Transient
    private List<HouseImage> list;
}
