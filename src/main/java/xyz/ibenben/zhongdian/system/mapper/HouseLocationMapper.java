package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.HouseLocation;

/**
 * 房屋位置Mapper类
 * 提供了一些基本的服务，如获取所有房屋信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface HouseLocationMapper extends BaseMapper<HouseLocation> {

}