package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.DiscountInfo;
import xyz.ibenben.zhongdian.system.form.DiscountInfoForm;

import java.util.List;

/**
 * 折扣信息Mapper类
 * 提供了一些基本的服务，如获取所有房屋折扣等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface DiscountInfoMapper extends BaseMapper<DiscountInfo> {
    /**
     * 根据条件查询列表
     *
     * @param discountInfoForm 条件
     * @return 列表
     */
    List<DiscountInfo> findAll(DiscountInfoForm discountInfoForm);
}