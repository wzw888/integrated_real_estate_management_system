package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.form.HouseImageForm;

import java.util.List;

/**
 * 房屋图像Mapper类
 * 提供了一些基本的服务。
 * 此类提供了所有系统里图像的处理功能
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface HouseImageMapper extends BaseMapper<HouseImage> {
    /**
     * 根据条件查询列表
     *
     * @param houseImageForm 条件
     * @return 列表
     */
    List<HouseImage> findAll(HouseImageForm houseImageForm);
}