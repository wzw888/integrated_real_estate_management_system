package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.PropertyOwnershipCertificate;
import xyz.ibenben.zhongdian.system.form.PropertyOwnershipCertificateForm;

import java.util.List;

/**
 * 产权Mapper类
 * 提供了一些基本的服务
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface PropertyOwnershipCertificateMapper extends BaseMapper<PropertyOwnershipCertificate> {
    /**
     * 根据条件查询列表
     *
     * @param propertyOwnershipCertificateForm 条件
     * @return 里恩
     */
    List<PropertyOwnershipCertificate> findAll(PropertyOwnershipCertificateForm propertyOwnershipCertificateForm);
}