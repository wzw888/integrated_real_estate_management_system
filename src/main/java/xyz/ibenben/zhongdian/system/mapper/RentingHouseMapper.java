package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.RentingHouse;
import xyz.ibenben.zhongdian.system.form.RentingHouseForm;

import java.util.List;

/**
 * 租赁房屋Mapper类
 * 提供了一些基本的服务。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface RentingHouseMapper extends BaseMapper<RentingHouse> {
    /**
     * 根据条件查询列表
     *
     * @param rentingHouseForm 条件
     * @return 列表
     */
    List<RentingHouse> findAll(RentingHouseForm rentingHouseForm);
}