package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.SuggestInfo;
import xyz.ibenben.zhongdian.system.form.SuggestInfoForm;

import java.util.List;

/**
 * 意见及建议记录Mapper类
 * 提供了一些基本的服务，如根据条件查询列表等方法。
 * 是用户在意见及建议时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface SuggestInfoMapper extends BaseMapper<SuggestInfo> {

    /**
     * 根据条件查询列表
     *
     * @param suggestInfoForm 条件
     * @return 列表
     */
    List<SuggestInfo> findAll(SuggestInfoForm suggestInfoForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    SuggestInfo findByKey(@Param("id") Long id);
}