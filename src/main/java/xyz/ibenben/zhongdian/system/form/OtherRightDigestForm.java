package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.RightTypeEnum;

/**
 * 他项权利摘要查询条件类
 * 提供了对拥有者，权利人等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class OtherRightDigestForm extends SearchForm {
    private static final long serialVersionUID = 4551767121190601428L;

    /**
     * 拥有者
     */
    protected Long ownerId;

    /**
     * 权利人
     */
    protected String righter;

    /**
     * 产权类型
     */
    protected RightTypeEnum type;

}
