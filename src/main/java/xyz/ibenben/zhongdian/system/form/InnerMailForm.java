package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.MailTypeEnum;

/**
 * 站内信查询条件类
 * 提供了对标题，站内信类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class InnerMailForm extends SearchForm {
    private static final long serialVersionUID = -4776031389494239203L;

    /**
     * 标题
     */
    protected String title;

    /**
     * 站内信类型 1后台公告 2前台公告 3后台私信 4前台私信
     */
    protected MailTypeEnum type;

    /**
     * 是否已读
     */
    protected Boolean isRead;

}
