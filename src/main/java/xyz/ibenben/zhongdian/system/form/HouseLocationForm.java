package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 房屋信息查询条件类
 * 提供了对房屋名称，室数量类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class HouseLocationForm extends SearchForm {

}
