package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.PayTypeEnum;

/**
 * 租赁房屋查询条件类
 * 提供了对价格，价格类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class RentingHouseForm extends SearchForm {
    private static final long serialVersionUID = 2520767505983089325L;
    /**
     * 价格字符串
     */
    private static final String PRICESTRING = "price";

    /**
     * 价格
     */
    protected Integer price;

    /**
     * 价格类型
     */
    protected PayTypeEnum priceType;

    /**
     * 联系电话
     */
    protected String phone;

    /**
     * 标题
     */
    protected String title;

}
