package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 产权查询条件类
 * 提供了对拥有者，建筑面积等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class PropertyOwnershipCertificateForm extends SearchForm {
    private static final long serialVersionUID = -4822871095668341996L;

    /**
     * 拥有者
     */
    protected String ownershiper;

    /**
     * 建筑面积
     */
    protected BigDecimal coveredArea;

    /**
     * 使用面积
     */
    protected BigDecimal useableArea;

    /**
     * 登记字号
     */
    protected String loginNumber;

}
