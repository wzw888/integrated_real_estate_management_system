package xyz.ibenben.zhongdian.common.handler;

import org.apache.ibatis.type.MappedTypes;
import xyz.ibenben.zhongdian.common.converter.BaseEnum;
import xyz.ibenben.zhongdian.common.converter.UniversalEnumHandler;
import xyz.ibenben.zhongdian.system.entity.enums.*;

/**
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@MappedTypes(value = {HouseStructureEnum.class, HouseTypeEnum.class,
        ImageTypeEnum.class, ChatTypeEnum.class, MailTypeEnum.class,
        OrientationEnum.class, StatusTypeEnum.class, RightTypeEnum.class,
        PayTypeEnum.class, ResourceTypeEnum.class, CompanyTypeEnum.class,
        DealRecordTypeEnum.class, StuffStatusEnum.class, CompanyStatusEnum.class,
        SuggestTypeEnum.class})
public class EnumTypeHandler<E extends BaseEnum<Integer>> extends UniversalEnumHandler<E> {
    /**
     * 转换枚举为Integer类型
     *
     * @param type
     */
    public EnumTypeHandler(Class<E> type) {
        super(type);
    }
}
