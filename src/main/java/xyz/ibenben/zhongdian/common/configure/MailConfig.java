package xyz.ibenben.zhongdian.common.configure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import java.util.Properties;

/**
 * 定义邮件发送配置
 *
 * @author chenjian
 * @since 2015年11月01日 下午2:05:37
 */
@Configuration
public class MailConfig {
    /* 用户名 */
    @Value("${emailsender}")
    private String sender;

    /* 密码 */
    @Value("${emailpassword}")
    private String password;

    /**
     * 创建发送器
     *
     * @return
     */
    @Bean(name = "JavaMailSender")
    public JavaMailSender getSender() {
        //设置发送邮件的信息
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setUsername(sender);
        javaMailSender.setHost("smtp.qq.com");
        javaMailSender.setPort(587);
        javaMailSender.setDefaultEncoding("UTF-8");
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "smtp.qq.com");
        props.setProperty("mail.smtp.auth", "true");
        //发送邮件
        javax.mail.Session session = javax.mail.Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(sender, password);
            }
        });
        javaMailSender.setSession(session);
        return javaMailSender;
    }

}