package xyz.ibenben.zhongdian.common.util.example.easy;

/**
 * 给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
 * 示例 1:
 * 输入: 123
 * 输出: 321
 * 示例 2:
 * 输入: -123
 * 输出: -321
 * 示例 3:
 * 输入: 120
 * 输出: 21
 * 注意:假设我们的环境只能存储得下 32 位的有符号整数，则其数值范围为 [−2^31,  2^31 − 1]。
 * 请根据这个假设，如果反转后整数溢出那么就返回 0。
 */
public class ReverseInteger {
    public static void main(String[] args) {
        ReverseInteger ri = new ReverseInteger();
        System.out.println(ri.reverse(-1230));
        System.out.println(ri.reverse2(-13200));
    }

    /**
     * 我自己写的解决方法
     *
     * @param x
     * @return
     */
    public int reverse(int x) {
        boolean isLessZero = false;
        if (x < 0) {
            isLessZero = true;
            x = Math.abs(x);
        }
        StringBuilder result = new StringBuilder();
        char[] chars = String.valueOf(x).toCharArray();
        for (int i = chars.length - 1; i >= 0; i--) {
            result.append(chars[i]);
        }
        try {
            int r = Integer.valueOf(result.toString());
            return isLessZero ? 0 - r : r;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 官方给出的解决方法
     *
     * @param x
     * @return
     */
    public int reverse2(int x) {
        int rev = 0;
        while (x != 0) {
            int pop = x % 10;
            x /= 10;
            if (rev > Integer.MAX_VALUE / 10 || (rev == Integer.MAX_VALUE / 10 && pop > 7)) {
                return 0;
            }
            if (rev < Integer.MIN_VALUE / 10 || (rev == Integer.MIN_VALUE / 10 && pop < -8)) {
                return 0;
            }
            rev = rev * 10 + pop;
        }
        return rev;
    }
}
