package xyz.ibenben.zhongdian.common.util;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 图片区域类
 *
 * @author chenjian
 */
@Setter
@Getter
@ToString
public class ImageArea {
    /**
     * 指定区域左上角横坐标
     */
    private int x;
    /**
     * 指定区域左上角纵坐标
     */
    private int y;
    /**
     * 指定区域宽度
     */
    private int width;
    /**
     * 指定区域高度
     */
    private int height;

    public ImageArea(int x, int y, int width, int height) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + height;
        result = prime * result + width;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ImageArea other = (ImageArea) obj;
        if (height != other.height) {
            return false;
        }
        if (width != other.width) {
            return false;
        }
        if (x != other.x) {
            return false;
        }
        return y == other.y;
    }

}
