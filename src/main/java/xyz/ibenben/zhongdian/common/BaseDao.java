package xyz.ibenben.zhongdian.common;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * Dao基类
 * 继承了tk的mapper，使继承此类的接口类，可以直接调用方法
 * 处理数据库的CRUD 基本方法
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public interface BaseDao<T> extends Mapper<T>, MySqlMapper<T> {

}
